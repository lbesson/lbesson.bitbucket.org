// From https://github.com/mxstbr/sirke.is
var adjectives = [ 'adventurous', 'affable', 'affectionate', 'agreeable', 'amazing', 'ambitious', 'amiable', 'amicable', 'amusing', 'artistic', 'awesome', 'brave', 'beautiful', 'bright', 'brilliant', 'calm', 'charming', 'cheerful', 'clever', 'communicative', 'compassionate', 'competent', 'conscientious', 'convivial', 'courageous', 'courteous', 'creative', 'cute', 'indecisive', 'determined', 'diligent', 'dynamic', 'easygoing', 'energetic', 'enthusiastic', 'exuberant', 'excellent', 'fabulous', 'fair-minded', 'faithful', 'fearless', 'friendly', 'funny', 'gentle', 'good', 'hard-working', 'helpful', 'honest', 'humorous', 'imaginative', 'independent', 'intellectual', 'intelligent', 'interesting', 'intuitive', 'inventive', 'joyous', 'kind', 'lively', 'lovable', 'loving', 'lovely', 'loyal', 'nice', 'optimistic', 'passionate', 'patient', 'persistent ', 'placid', 'playful', 'plucky', 'pretty', 'polite', 'powerful', 'practical', 'quick-witted', 'rational', 'reliable', 'resourceful', 'romantic', 'self-disciplined', 'sensible', 'sensitive', 'smart', 'sincere', 'sociable', 'sweet', 'sympathetic', 'talented', 'thoughtful', 'tidy', 'tolerant', 'understanding', 'warm', 'warmhearted', 'willing', 'witty', 'wonderful' ];
var copy = adjectives.slice();
var colours = [ '#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#03A9F4', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107', '#FF9800', '#FF5722' ];
var pauseBetweenAdjectives = 4000;
var wrapper;
var background;
var secondBackground;
var elem;
var sheis;
var progressBar;
var label;
var backgroundElem;
var prevBackgroundElem;
var newAdjective;

window.onload = function() {
	// Define variables
	wrapper = document.getElementById('wrapper');
	background = document.getElementById('background');
	secondBackground = document.getElementById('second__background');
	elem = document.getElementById('adjective');
	sheis = document.getElementById('sheis');
	progressBar = document.getElementById('progressbar');
	backgroundElem = secondBackground;

    // Start app
    newAdjective = setAdjective();
	// Update progressbar every 10ms
	setInterval(updateProgressBar, 10);
};

// Shows a new adjective
function setAdjective(prevBackgroundElem) {
	// If all adjectives have been shown, fill the array back up
	if (adjectives.length === 0) {
		adjectives = copy.slice();
	}
	// Get a random adjective
	var adjective = adjectives[Math.floor(Math.random() * adjectives.length)];
	// Then delete it, as to not repeat an adjective
	adjectives.splice(adjectives.indexOf(adjective), 1);
	// Get a random colour
	var colour = colours[Math.floor(Math.random() * colours.length)];
	// Set background and previous background element variables to fade between background colors
	if (backgroundElem === background) {
		backgroundElem = secondBackground;
		prevBackgroundElem = background;
	} else {
		backgroundElem = background;
		prevBackgroundElem = secondBackground;
	}
	// Fade to the new background colour
	backgroundElem.style.backgroundColor = colour;
	document.body.style.backgroundColor = colour;
	backgroundElem.style.opacity = 1;
	prevBackgroundElem.style.opacity = 0;
	// Set the text colour to either black or white, depending on the background colour
	var textColour = getContrast(colour);
	elem.style.color = textColour;
	sheis.style.color = textColour;
	progressbar.style.backgroundColor = textColour;
	// Set the text to the random adjective
	while( elem.firstChild ) {
	    elem.removeChild( elem.firstChild );
	}
	elem.appendChild(document.createTextNode(adjective));
	newAdjective = new Timer(setAdjective, pauseBetweenAdjectives);
    return newAdjective;
}

// Sets the progressbar width
function updateProgressBar() {
	var percentage = 100 - (newAdjective.getTimeLeft() / pauseBetweenAdjectives * 100);
	progressBar.style.width = percentage + '%';
}

// Returns colour with highest contrast in relation to hexcolour
function getContrast(hexcolour){
	hexcolour = hexcolour.substr(1, 7);
	var r = parseInt(hexcolour.substr(0,2),16);
	var g = parseInt(hexcolour.substr(2,2),16);
	var b = parseInt(hexcolour.substr(4,2),16);
	var contrast = ((r*299)+(g*587)+(b*114))/1000;
	return (contrast >= 128) ? 'black' : 'white';
}

// Adds useful functions to a setTimeout
var Timer = function(callback, delay) {
    var id, started, remaining = delay, running

    // Starts a timer
    this.start = function() {
        running = true;
        started = new Date();
        id = setTimeout(callback, remaining);
    }

    // Pauses a timer
    this.pause = function() {
        running = false;
        clearTimeout(id);
        remaining -= new Date() - started;
    }

    // Returns leftover time
    this.getTimeLeft = function() {
        if (running) {
            this.pause();
            this.start();
        }
        return remaining;
    }

    this.start();
}
