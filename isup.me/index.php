<!DOCTYPE html>
<!-- © and (C) Lilian Besson, 2013 : for //lbesson.bitbucket.io/isup.me -->
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>isup.me</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Clone de isup.me" />
    <meta name="author" content="Lilian Besson" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5, user-scalable=yes" />

    <link rel="shortcut icon" type="image/x-icon" href="http://perso.crans.org/besson/_static/.favicon.ico?1" />
    <link rel="stylesheet" media="screen and (min-width: 380px)" type="text/css"
    	  href="http://perso.crans.org/besson/_static/nprogress.css?1" />
    <link rel="stylesheet" media="screen and (min-width: 680px)" type="text/css"
    	  href="http://perso.crans.org/besson/_static/forkit.css?1" />
    <link rel="stylesheet" type="text/css" href="http://perso.crans.org/besson/_static/isup.css?1" />

    <meta name="google-site-verification" content="wU18kCoTfbukrhACrsDyaSBfKRFaVErblritzOhsBOU" />
</head>
<body>
    <div id="loading">
    	Chargement en cours...
    </div>

    <script type="text/javascript" src="http://perso.crans.org/besson/_static/jquery.js?1"></script>    
    <script type="text/javascript" src="http://perso.crans.org/besson/_static/nprogress.js?1"></script>
    <script type="text/javascript">NProgress.start();</script>

    <script type="text/javascript">
      var wt = window.document.title;
      console.log("[INFO] var wt = " + wt);
      var wl = window.location;
      console.log("[INFO] var wl = " + wl);
      function clearDomainInput(e) {
        if (e.cleared) { return; }
        e.cleared = true;
        e.value = "";
        e.style.color = "#000";
      }
      function formSubmit() {
        domain = document.getElementById("domain_input").value;
        if (window.confirm("Vérifier le nom de domaine '" + domain + "' ?")) {
	        console.log("[INFO] domain = " + domain + ", new window.document.title is '" + wt + "/q/" + domain + "'");
	        window.document.title = wt + "/q/" + domain;
	        console.log("[INFO] New window.location is '" + wl + "/q/" + domain + "'");
	        window.location = wl + "/q/" + domain;
	        return false;
	    } else {
	    	document.getElementById("loading").text("D'accord, on ne fait rien. (c'est dommage quand même !)");
	    	console.log("[INFO] Doing nothing. What a shame !");
	    }
      }
    </script>

    <div id="container">
      <form method="get" id="downform" name="downform" action="/q" onsubmit="return formSubmit()">
        Est-ce que
        <input type="text" name="domain" id="domain_input" value="monip.org" onclick="clearDomainInput(this);" />
        est <i>inaccessible</i> pour tout le monde<br/>
        <a href="#" onclick="formSubmit();">ou juste pour moi ?</a>
        <input type="submit" style="display: none;" />
      </form>
    </div>

    <div id="pkill">
		<?php
			exec("pkill -9 ping"); // Just to be sure the page is not accumulating dead locked commands.
		?>
    </div>

    <br><br><hr>

    <div id="copyright">
    	<small>
    		Cette page est un clone de <a href="http://isup.me/">isup.me</a>, traduit en français.
    		<br>
    		© Lilian Besson, 2014
    	</small>
    </div>
    
    <div id="divforkit">
      <small>
	      <a class="forkit" data-text="Sur BitBucket ?" data-text-detached="Tout est OpenSource !"
	       href="https://bitbucket.org/lbesson/lbesson.bitbucket.org/src/master/isup.me/">
	      	<img id="imgforkit" alt="Code source de cette page ?" />
	      </a>
      </small>
    </div>
<footer>
  <script async type="text/javascript" src="http://perso.crans.org/besson/_static/forkit.js?1"></script>
  <script type="text/javascript">
    NProgress.inc();
    $(document).ready(function(){
        (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,"script","//www.google-analytics.com/analytics.js","ga");
        ga("create", "UA-38514290-14", "bitbucket.org");
        ga("send", "pageview");
        console.log( "[GA] Sending with ID=UA-38514290-14, DOMAIN=bitbucket.org" );
        setTimeout(function() {
          NProgress.done();
          $("#loading").text("");
        }, 2000);
      });
  </script>
</footer>
</body>
</html>