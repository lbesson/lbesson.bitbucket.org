<!DOCTYPE html>
<!-- © and (C) Lilian Besson, 2013 : for //lbesson.bitbucket.io/isup.me -->
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>isup.me</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Clone de isup.me" />
    <meta name="author" content="Lilian Besson" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5, user-scalable=yes" />

    <link rel="shortcut icon" type="image/x-icon" href="http://perso.crans.org/besson/_static/.favicon.ico?1" />
    <link rel="stylesheet" media="screen and (min-width: 380px)" type="text/css"
    	  href="http://perso.crans.org/besson/_static/nprogress.css?1" />
    <link rel="stylesheet" media="screen and (min-width: 680px)" type="text/css"
    	  href="http://perso.crans.org/besson/_static/forkit.css?1" />
    <link rel="stylesheet" type="text/css" href="http://perso.crans.org/besson/_static/isup.css?1" />

    <meta name="google-site-verification" content="wU18kCoTfbukrhACrsDyaSBfKRFaVErblritzOhsBOU" />
</head>
<body>
    <div id="loading">
    	Chargement en cours...
    </div>

    <script type="text/javascript" src="http://perso.crans.org/besson/_static/jquery.js?1"></script>
    
    <script type="text/javascript" src="http://perso.crans.org/besson/_static/nprogress.js?1"></script>
    <script type="text/javascript">NProgress.start();</script>
    

    <div id="container">
    <?php
		require_once '../ServerPing.php';
		function ping($url) {
			$serverPing=new ServerPing();
			$serverPing->send($url, 1);
			if ($serverPing->isAlive()) {
				echo "<b>C'est juste vous.</b> <a href=\"http://".$url."\">".$url."</a> semble opérationnel <i>vu d'ici</i> !<br>";
    			// echo "<hr>Ping <pre>".$serverPing->getOutput()."</pre>";
    			if ($serverPing->getAverage()<100) {
    				echo "<i>".$serverPing->getAverage()."</i> ms : semble en bonne santé <i>vu d'ici</i>.";
    			} else {
    				echo "<i>".$serverPing->getAverage()."</i> ms : c'est <b>vraiment</b> lent. <i>Quelle honte !</i>";
    			}
            } else {
                echo "Peut-être que ce n'est pas juste vous. <a href=\"http://".$url."\">".$url."</a> semble inaccessible <i>vu d'ici</i>...<br>";
            }
			echo "<hr>";
		}
    $s = $_SERVER['QUERY_STRING']; // $s = d=domain
    $domain = substr($s, 2); 
    ping($domain);
    ?>
    </div>
	
	<br><br>
    <div id="back">
    	<a href="..">Testez un autre domaine ?</a>
    </div>

    <br><br><hr>

    <div id="copyright">
    	<small>
    		Cette page est un clone de <a href="http://isup.me/">isup.me</a>, traduit en français.
    		<br>
    		© Lilian Besson, 2014
    	</small>
    </div>
    
    <div id="divforkit">
      <small>
	      <a class="forkit" data-text="Sur BitBucket ?" data-text-detached="Tout est OpenSource !"
	       href="https://bitbucket.org/lbesson/lbesson.bitbucket.org/src/master/isup.me/">
	      	<img id="imgforkit" alt="Code source de cette page ?" />
	      </a>
      </small>
    </div>
<footer>
  <script async type="text/javascript" src="http://perso.crans.org/besson/_static/forkit.js?1"></script>
  <script type="text/javascript">
    NProgress.inc();
    $(document).ready(function(){
        (function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,"script","//www.google-analytics.com/analytics.js","ga");
        ga("create", "UA-38514290-14", "bitbucket.org");
        ga("send", "pageview");
        console.log( "[GA] Sending with ID=UA-38514290-14, DOMAIN=bitbucket.org" );
        setTimeout(function() {
          NProgress.done();
          $("#loading").text("");
        }, 2500);
      });
  </script>
</footer>
</body>
</html>