## ["Particle" homepage](//lbesson.bitbucket.io/particle_homepage/)

This folder contains the [HTML](index.html), [javascript](js/) and [CSS3](css) code for the [particle homepage](//lbesson.bitbucket.io/particle_homepage/). The particles moving in the background are configured with [this JSON file](particles.json).

It is heavily inspired from [this page](https://tylernickerson.com/), and only small modifications were done. Thanks to [Tyler Nickerson](https://tylernickerson.com/) for his nice design!